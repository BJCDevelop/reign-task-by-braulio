//Imports of all dependencies and packages
import express, {Application, Request, Response, NextFunction} from 'express'
import axios from 'axios'
import cron from 'node-cron'
import mongoose from 'mongoose'
import "dotenv/config";

const app: Application = express()

// Setting up variables
var date: Date = new Date()
var getData: any = {}

// Ports
app.set('port', process.env.PORT || 4000);

// Endpoints
app.get('/', (req: Request, res: Response, next: NextFunction) => {
    res.send(`Last updated was ${date}`)
})
app.use('/get-hits', (req: Request, res: Response) => {
    res.send(getData)
})

// Logs and Scheduling
app.listen(app.get('port'), () => {
    console.log('Server running')

    cron.schedule('0-59 * * * *', () => {
        // Getting data
        axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .then(response => {
            getData = response.data.hits
            date = new Date()
            console.log(`Last data update was on ${date.toISOString()}`)
            // Set up the url connection
            mongoose.connect(`${process.env.MONGO_URI}`);

            // Start connection
            const db = mongoose.connection;
            db.on("error", console.error.bind(console, "connection error: "));
            db.once("open", function () {
                console.log("Connected successfully");
            });

            var dbo = db.useDb('reign_task')

            getData.map((element: { objectID: string; }) => {
                dbo.collection('hits').updateOne({'objectID': element.objectID}, {$set: element}, {upsert: true})
            });

            db.close()
        })
        .catch(error => {
            console.log(error);
        });
    },
    {
        scheduled: true
    })
})